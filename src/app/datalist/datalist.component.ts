import { Component, OnInit } from '@angular/core';
import { ExService } from '../ex.service';

@Component({
  selector: 'app-datalist',
  templateUrl: './datalist.component.html',
  styleUrls: ['./datalist.component.css']
})
export class DatalistComponent implements OnInit {
  data:any=[{"name":"alka"},{"name":"shweta"},{"name":"abhi"}];
  data1:any=[];
  constructor(private service:ExService) { }

  ngOnInit() {
  }
 Example(){
   this.service.getData().subscribe(data=>{
     this.data1=data;
   },err=>{
     console.log(err);
   })
 }
  
}
