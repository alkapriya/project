import { Component, OnInit } from '@angular/core';
import {FormGroup,FormBuilder,Validators} from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  form:FormGroup;
  name:string;
  email:string;
  pass:string;
  constructor(private fb:FormBuilder) { }

  ngOnInit() {
    this.form=this.fb.group({
      name:['',Validators.required],
      email:['',[Validators.required,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"')]],
      password:['',[Validators.required,Validators.maxLength(6),Validators.minLength(4)]]
    })
  }
  save(){
    console.log(this.form.value);
  }
  refresh(){
    this.name="";
    this.email="";
    this.pass="";
  }
}
