import { TestBed, inject } from '@angular/core/testing';

import { ExService } from './ex.service';

describe('ExService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExService]
    });
  });

  it('should be created', inject([ExService], (service: ExService) => {
    expect(service).toBeTruthy();
  }));
});
