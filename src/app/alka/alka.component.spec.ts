import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlkaComponent } from './alka.component';

describe('AlkaComponent', () => {
  let component: AlkaComponent;
  let fixture: ComponentFixture<AlkaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlkaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlkaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
